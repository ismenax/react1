import React from 'react';

// komponent funkcyjny
// const ListElem = (props.name) => {
//     return(
//         <div>
//             <li>{props.name}</li>
//         </div>
//     )
// }


// komponent klasowy
class ListElem extends React.Component {
    render() {
        return (
            <div>
                <li>{this.props.name}</li>
            </div>
        )
    }
}

export default ListElem;