import ListElem from '../../components/ListElem'

const homepage = ({ title }) => {
    const myTitle = <h1 className="myFirstTitle">{title}</h1>;
    return (
        <div>
            {myTitle}
            <ul>
                <ListElem name='dish 1' />
                <ListElem name='dish 2' />
                <ListElem name='dish 3' />
                <ListElem name='dish 4' />
                <ListElem name='dish 5' />
            </ul>
        </div>
    );
}

export default homepage;
